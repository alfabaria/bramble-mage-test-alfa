# Bramble Mage test Alfa

Bramble technical test result


### Admin User Credential

```console
username : xavi
password : persipura
```

### Question Number 3 (*debug match() method*)

method **match()** located in file **app/code/core/Mage/Core/Controller/Varien/Router/Standard.php** is used for handles request routing in Magento.
the **match()** method contains the bulk of the logic that tries to match a URL to a module, controller, and action,
for some reason your controller is not being used and you’re getting an 404 page instead of whatever functionality you’re expecting

