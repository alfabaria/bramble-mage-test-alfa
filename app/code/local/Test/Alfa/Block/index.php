<?php   
class Test_Alfa_Block_Index extends Mage_Core_Block_Template{   
public function index()
{
	$products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('sort_order')
			->addAttributeToFilter('sort_order', array('gt' => '0'));
	return $products;
}

}