<?php
 
$commnets = array(
    array(
        'comment' => 'Test data 1',
        'description' => 'This is test data 1 for custom table.',
    ),
    array(
        'comment' => 'Test data 2',
        'description' => 'This is test data 2 for custom table.',
    ),
	array(
        'comment' => 'Test data 3',
        'description' => 'This is test data 3 for custom table.',
    ),
	array(
        'comment' => 'Test data 4',
        'description' => 'This is test data 4 for custom table.',
    ),
	array(
        'comment' => 'Test data 5',
        'description' => 'This is test data 5 for custom table.',
    ),
	
);
 
foreach ($commnets as $commnet) {
    Mage::getModel('test_alfa/comment')
        ->setData($commnet)
        ->save();
}