<?php
class Test_Alfa_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {

      $this->loadLayout();   
      $this->getLayout()->getBlock("head")->setTitle($this->__("Alfa"));
            $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
           ));

      $breadcrumbs->addCrumb("titlename", array(
                "label" => $this->__("Alfa"),
                "title" => $this->__("Alfa")
           ));

      $this->renderLayout();

    }
}