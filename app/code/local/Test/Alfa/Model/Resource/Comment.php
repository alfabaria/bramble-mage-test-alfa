<?php
 
class Test_Alfa_Model_Resource_Comment extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('test_alfa/comment', 'comment_id');
    }
}